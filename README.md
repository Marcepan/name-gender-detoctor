# Name gender detection web app

## Overview
The application offers two variants of checking the gender according to the given name or names. 
The application provides the rest api and returns the query results in the JSON form.
Matching is done with the help of the set of names extracted from the dataset available [here](https://data.world/len/us-first-names-database).
The application can recognize a name from a collection of 13959 male names and 18993 female names.

## Defined endpoints
[/names](http://localhost:8080/) - returns all available names by gender

[/names/gender](http://localhost:8080/) - returns the detected gender for the given string - variant with the analysis of all names

[/names/gender/1](http://localhost:8080/) - returns the detected gender for the given string - variant with only the first name analysis 

## Dataset
A dataset from the US First Names Database was used for gender detection. 
It is available on the platform [data.world](https://data.world/len/us-first-names-database).
All names are from Social Security card applications for births that occurred in the United States after 1879 as of the end of February 2016.

Names for use in the application have been extracted from the SSA_Names_DB set and grouped into separate files according to gender.
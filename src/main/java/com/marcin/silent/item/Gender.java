package com.marcin.silent.item;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.ArrayList;
import java.util.List;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Gender {
    MALE("Male"),
    FEMALE("Female"),
    INCONCLUSIVE("Inconclusive");

    private String gender;
    private List<String> names;

    Gender(String name) {
        this.gender = name;
        names = new ArrayList<>();
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<String> getNames() {
        return names;
    }

    public void setNames(List<String> names) {
        this.names = names;
    }
}

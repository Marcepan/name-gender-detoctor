package com.marcin.silent.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.marcin.silent.detectors.AllTokensGenderDetector;
import com.marcin.silent.detectors.GenderDetector;
import com.marcin.silent.detectors.SingleTokenGenderDetector;
import com.marcin.silent.item.Gender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NameService {

    @Value("${name.files.malePath}")
    private String maleFilePath;

    @Value("${name.files.femalePath}")
    private String femaleFilePath;

    private GenderDetector genderDetector;

    public NameService() {
        this.genderDetector = new AllTokensGenderDetector();
    }

    public void setDetectorVariant(int variantNumber) {
        if(variantNumber == 1) {
            genderDetector = new SingleTokenGenderDetector();
        }
    }

    public Gender detectGender(String name) {
        return genderDetector.detectGender(name);
    }

    public List<Gender> getAllNames() {
        try {
            List<String> maleNames = Files.lines(Path.of(maleFilePath)).collect(Collectors.toList());
            Gender.MALE.setNames(maleNames);
            List<String> femaleNames = Files.lines(Path.of(femaleFilePath)).collect(Collectors.toList());
            Gender.FEMALE.setNames(femaleNames);
        } catch (IOException e) {
            e.printStackTrace();
            return List.of();
        }

        return List.of(Gender.MALE, Gender.FEMALE);
    }
}

package com.marcin.silent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NameGenderDetectorApplication {

    public static void main(String[] args) {
        SpringApplication.run(NameGenderDetectorApplication.class, args);
    }

}

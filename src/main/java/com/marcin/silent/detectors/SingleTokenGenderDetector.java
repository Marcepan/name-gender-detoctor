package com.marcin.silent.detectors;

import com.marcin.silent.item.Gender;
import org.springframework.stereotype.Component;

@Component
public class SingleTokenGenderDetector extends GenderDetector {

    @Override
    public Gender detectGender(String name) {
        if(name.isBlank()) {
            return Gender.INCONCLUSIVE;
        }
        String firstName = name.split(" ")[0];
        if(isNameInFile(maleFilePath, firstName)) {
            return Gender.MALE;
        } else if(isNameInFile(femaleFilePath, firstName)) {
            return Gender.FEMALE;
        }
        return Gender.INCONCLUSIVE;
    }
}

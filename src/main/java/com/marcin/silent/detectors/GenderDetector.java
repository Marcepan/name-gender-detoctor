package com.marcin.silent.detectors;

import com.marcin.silent.item.Gender;
import org.springframework.beans.factory.annotation.Value;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public abstract class GenderDetector {

    @Value("${name.files.malePath}")
    protected String maleFilePath;

    @Value("${name.files.femalePath}")
    protected String femaleFilePath;

    public abstract Gender detectGender(String name);

    protected boolean isNameInFile(String fileName, String name) {
        try(FileReader reader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader((reader))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals(name)) {
                    return true;
                }
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return false;
        }
        return false;
    }
}

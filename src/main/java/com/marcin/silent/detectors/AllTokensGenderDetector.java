package com.marcin.silent.detectors;

import com.marcin.silent.item.Gender;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AllTokensGenderDetector extends GenderDetector {

    @Override
    public Gender detectGender(String name) {
        if(name.isBlank()) {
            return Gender.INCONCLUSIVE;
        }
        List<String> names = List.of(name.split(" "));
        long male = names.stream()
                .filter(n -> super.isNameInFile(maleFilePath, n))
                .count();
        long female = names.stream()
                .filter(n -> super.isNameInFile(femaleFilePath, n))
                .count();
        if(male > female) {
            return Gender.MALE;
        } else if(female > male) {
            return Gender.FEMALE;
        }
        return Gender.INCONCLUSIVE;
    }
}

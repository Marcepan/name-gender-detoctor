package com.marcin.silent.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:application.properties")
@ConfigurationProperties(prefix = "name.files")
public class NameFilesConfig {

    private String malePath;
    private String femalePath;

    public String getMalePath() {
        return malePath;
    }

    public void setMalePath(String malePath) {
        this.malePath = malePath;
    }

    public String getFemalePath() {
        return femalePath;
    }

    public void setFemalePath(String femalePath) {
        this.femalePath = femalePath;
    }
}

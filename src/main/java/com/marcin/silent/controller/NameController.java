package com.marcin.silent.controller;

import com.marcin.silent.item.Gender;
import com.marcin.silent.service.NameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/names")
public class NameController {

    @Autowired
    NameService nameService;

    @ResponseStatus(HttpStatus.OK)
    @PostMapping(value = "/gender/{variant}", produces = MediaType.APPLICATION_JSON_VALUE)
    public String getGender(@RequestBody String name, @Nullable @PathVariable Integer variant) {
        if(variant != null) {
            nameService.setDetectorVariant(variant);
        }
        return nameService.detectGender(name).getGender();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Gender> getNames() {
        return nameService.getAllNames();
    }
}

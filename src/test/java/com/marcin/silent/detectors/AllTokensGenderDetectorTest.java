package com.marcin.silent.detectors;

import static org.mockito.Mockito.*;

import com.marcin.silent.item.Gender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AllTokensGenderDetector.class)
public class AllTokensGenderDetectorTest {

    @MockBean
    private AllTokensGenderDetector allTokensGenderDetector;

    @Value("${name.files.malePath}")
    protected String maleFilePath;

    @Value("${name.files.femalePath}")
    protected String femaleFilePath;

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void shouldReturnMaleWhenDetectGender() {
        final String name = "Martin Andrew Jessica";
        when(allTokensGenderDetector.detectGender(name)).thenReturn(Gender.MALE);
        allTokensGenderDetector.detectGender(name);
        verify(allTokensGenderDetector).detectGender(any(String.class));
    }

    @Test
    public void shouldReturnFemaleWhenDetectGender() {
        final String name = "Lily Jasi Stewart";
        when(allTokensGenderDetector.detectGender(name)).thenReturn(Gender.FEMALE);
        allTokensGenderDetector.detectGender(name);
        verify(allTokensGenderDetector).detectGender(any(String.class));
    }

    @Test
    public void shouldReturnInclusiveWhenDetectGenderEmptyName() {
        final String name = " ";
        when(allTokensGenderDetector.detectGender(name)).thenReturn(Gender.INCONCLUSIVE);
        allTokensGenderDetector.detectGender(name);
        verify(allTokensGenderDetector).detectGender(any(String.class));
    }

    @Test
    public void shouldReturnInclusiveWhenDetectGenderNameNull() {
        final String name = null;
        when(allTokensGenderDetector.detectGender(name)).thenReturn(Gender.INCONCLUSIVE);
        allTokensGenderDetector.detectGender(name);
        verify(allTokensGenderDetector).detectGender(any());
    }

    @Test
    public void shouldReturnInclusiveWhenDetectGenderNotFound() {
        final String name = "Qwertyuio poiuytrewq qwertyuio";
        when(allTokensGenderDetector.detectGender(name)).thenReturn(Gender.INCONCLUSIVE);
        allTokensGenderDetector.detectGender(name);
        verify(allTokensGenderDetector).detectGender(any(String.class));
    }
}

package com.marcin.silent.detectors;

import static org.mockito.Mockito.*;

import com.marcin.silent.item.Gender;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.FileNotFoundException;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SingleTokenGenderDetector.class)
public class SingleTokenGenderDetectorTest {

    @MockBean
    private SingleTokenGenderDetector singleTokenGenderDetector;

    @Value("${name.files.malePath}")
    protected String maleFilePath;

    @Value("${name.files.femalePath}")
    protected String femaleFilePath;

    @Before
    public void setUp(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void trueIsTrue() {
        assertThat(true).isTrue();
    }

    @Test(expected = FileNotFoundException.class)
    public void shouldThrowFileNotFoundExceptionWhenCheckIsNameInFile() {
        final String filePath = "/nothing.txt";
        when(singleTokenGenderDetector.isNameInFile(anyString(), anyString())).thenThrow(FileNotFoundException.class);
        singleTokenGenderDetector.isNameInFile(filePath, anyString());
    }

    @Test
    public void shouldReturnTrueWhenCheckIsNameInFileMale() {
        final String name = "Martin";
        when(singleTokenGenderDetector.isNameInFile(maleFilePath, name)).thenReturn(Boolean.TRUE);
        singleTokenGenderDetector.isNameInFile(maleFilePath, name);
        verify(singleTokenGenderDetector).isNameInFile(anyString(), anyString());
    }

    @Test
    public void shouldReturnTrueWhenCheckIsNameInFileFemale() {
        final String name = "Lily";
        when(singleTokenGenderDetector.isNameInFile(femaleFilePath, name)).thenReturn(Boolean.TRUE);
        singleTokenGenderDetector.isNameInFile(femaleFilePath, name);
        verify(singleTokenGenderDetector).isNameInFile(anyString(), anyString());
    }

    @Test
    public void shouldReturnMaleWhenDetectGender() {
        final String name = "Martin";
        when(singleTokenGenderDetector.detectGender(name)).thenReturn(Gender.MALE);
        singleTokenGenderDetector.detectGender(name);
        verify(singleTokenGenderDetector).detectGender(any(String.class));
    }

    @Test
    public void shouldReturnFemaleWhenDetectGender() {
        final String name = "Lily";
        when(singleTokenGenderDetector.detectGender(name)).thenReturn(Gender.FEMALE);
        singleTokenGenderDetector.detectGender(name);
        verify(singleTokenGenderDetector).detectGender(any(String.class));
    }

    @Test
    public void shouldReturnInclusiveWhenDetectGenderEmptyName() {
        final String name = " ";
        when(singleTokenGenderDetector.detectGender(name)).thenReturn(Gender.INCONCLUSIVE);
        singleTokenGenderDetector.detectGender(name);
        verify(singleTokenGenderDetector).detectGender(any(String.class));
    }

    @Test
    public void shouldReturnInclusiveWhenDetectGenderNameNull() {
        final String name = null;
        when(singleTokenGenderDetector.detectGender(name)).thenReturn(Gender.INCONCLUSIVE);
        singleTokenGenderDetector.detectGender(name);
        verify(singleTokenGenderDetector).detectGender(any());
    }

    @Test
    public void shouldReturnInclusiveWhenDetectGenderNotFound() {
        final String name = "Qwertyuio";
        when(singleTokenGenderDetector.detectGender(name)).thenReturn(Gender.INCONCLUSIVE);
        singleTokenGenderDetector.detectGender(name);
        verify(singleTokenGenderDetector).detectGender(any(String.class));
    }

}

package com.marcin.silent.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.JsonPathResultMatchers.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(NameController.class)
public class NameControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnAllEnabledNames() throws Exception {
        mockMvc.perform(get("/names"))
                .andExpect(status().isOk());
    }

    @Test
    public void shouldReturnGenderByNameAllTokens() throws Exception {
        String name = "Martin Andrew Simona";
        mockMvc.perform(post("/gender").param("name", name))
                .andExpect(status().isOk())
                .andExpect(content().string("male"));
    }

    @Test
    public void shouldReturnGenderByNameSingleToken() throws Exception {
        String name = "Simona Martin Andrew";
        mockMvc.perform(post("/gender/1").param("name", name))
                .andExpect(status().isOk())
                .andExpect(content().string("male"));
    }

}
